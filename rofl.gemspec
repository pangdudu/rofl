Gem::Specification.new do |s|
  s.name = %q{rofl}
  s.version = "0.1.5"

  s.specification_version = 2 if s.respond_to? :specification_version=

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-07-29}
  s.default_executable = %q{rofl}
  s.description = %q{My logger wrapper, includes funkyness.}
  s.email = %q{pangdudu@github}
  s.executables = ["rofl"]
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "bin/rofl", "lib/rofl.rb", "lib/rofl_trace.rb", "lib/test_rofl.rb"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/rofl}
  #s.rubyforge_project = %q{rofl}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{rofl! peace.}
end
